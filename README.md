# The purpose of this project is to simply provide minimal docker images for running (different versions of) `tezos-node`.

Cf. https://gitlab.com/philippewang.info/tezos-node/container_registry/2009274

Usage example:

```bash
HOST_DIR=/tmp/tezos-node
INDOCKER_DIR=/.tezos-node
HOST_P2P_PORT=9732
HOST_RPC_PORT=8732
INDOCKER_P2P_PORT=9732
INDOCKER_RPC_PORT=8732
TEZOS_NODE_VERSION=v10.1
sudo docker run \
   -v ${HOST_DIR}:${INDOCKER_DIR} \
   -p ${HOST_RPC_PORT}:${INDOCKER_RPC_PORT} \
   -p ${HOST_P2P_PORT}:${INDOCKER_P2P_PORT} \
   registry.gitlab.com/philippewang.info/tezos-node/tezos-node:${TEZOS_NODE_VERSION} \
   run \
   --network mainnet \
   --net-addr=0.0.0.0:${INDOCKER_P2P_PORT} \
   --rpc-addr=0.0.0.0:${INDOCKER_RPC_PORT} \
   --history-mode=archive \
   --data-dir=${INDOCKER_DIR}
```

You can also "simply" use the `Dockerfile` directly.
```bash
HOST_DIR=/tmp/tezos-node
INDOCKER_DIR=/.tezos-node
HOST_P2P_PORT=9732
HOST_RPC_PORT=8732
INDOCKER_P2P_PORT=9732
INDOCKER_RPC_PORT=8732
TEZOS_NODE_VERSION=v10.1
git clone https://gitlab.com/philippewang.info/tezos-node.git
cd tezos-node
git checkout ${TEZOS_NODE_VERSION} # or edit Dockerfile to have make it have the version you need (don't forget to edit the base image and give it the hash you'll find in for `build_deps_image_version` from https://gitlab.com/tezos/tezos/-/blob/master/.gitlab-ci.yml -- replace `master` by the octez branch you want to use)
sudo docker build -t tezos-node-${TEZOS_NODE_VERSION} . 
sudo docker run \
   -v ${HOST_DIR}:${INDOCKER_DIR} \
   -p ${HOST_RPC_PORT}:${INDOCKER_RPC_PORT} \
   -p ${HOST_P2P_PORT}:${INDOCKER_P2P_PORT} \
   tezos-node-${TEZOS_NODE_VERSION} \
   run \
   --network mainnet \
   --net-addr=0.0.0.0:${INDOCKER_P2P_PORT} \
   --rpc-addr=0.0.0.0:${INDOCKER_RPC_PORT} \
   --history-mode=archive \
   --data-dir=${INDOCKER_DIR}
``` 

# The maintenance of this very simple project might be stopped at any time. 
